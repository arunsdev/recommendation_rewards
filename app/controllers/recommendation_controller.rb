class RecommendationController < ApplicationController
  def index
  end

  def create
  	uploaded_file = params[:file][:import_file]
    helpers.read_file(uploaded_file)
    redirect_to points_path
  end

  def show
    data = {}
    User.all.each{ |p| data[p.name] = p.points}
    @data = data.to_s
  end
end
