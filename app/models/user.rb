class User < ApplicationRecord
  belongs_to :parent, class_name: 'User', foreign_key: 'recommending_user_id', optional: true

  def ancestors
    node, nodes = self, []
    nodes << node = node.parent while node.parent
    nodes
  end
end