module RecommendationHelper
  def read_file(file)
    File.foreach(file.path).with_index do |lines, index|
      line_split = lines.strip.split
      action = line_split[1]
      case action
      when 'recommends'
        User.create(name: line_split[0], points: 0, is_accepted: true) if User.find_by(name: line_split[0]).nil?
        user1 = User.find_by(name: line_split[0])
        if line_split.length == 3
          User.create(name: line_split[2], points: 0, recommending_user_id: user1.id, is_accepted: false) if User.find_by(name: line_split[2]).nil?
        end
      when 'accepts'
        user = User.find_by(name: line_split[0])
        if user.present? && user.is_accepted == false
          user.update(is_accepted: true)
          recommends = user.ancestors
          i = 1.0
          recommends.each do |f|
            f.update(points: f.points + i)
            i /= 2.0
          end
        end
      end

    end
  end
end