Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/' => "recommendation#index"
  post '/' => "recommendation#create"
  get '/points' => "recommendation#show"
end
