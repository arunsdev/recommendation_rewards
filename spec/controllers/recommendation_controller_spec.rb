require 'rails_helper'

RSpec.describe RecommendationController, type: :controller do
  describe 'GET index' do
    it 'has a 200 status code' do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe  do
    let(:file) { fixture_file_upload('/test.txt') }
    it 'responds to text file as input' do
      post :show, params: { file: { import_file: file } }
      # Using have_http_status instead of be_success as it is being deprecated in Rails 6.0
      expect(response).to have_http_status(200)
    end

    it 'returns response as html' do
      post :show, params: { file: { import_file: file } }
      expect(response.content_type).to eq 'text/html'
    end

    it 'returns invalid if the input file is not .txt format' do
      filename = file.original_filename
      expect(filename).to include('.txt')
    end

    it 'returns invalid if file uploaded is empty' do
      a = file.read
      expect(a).to_not eql('')
    end

    it 'returns invalid if there is no specific valid action' do
      File.foreach(file.path).each do |lines|
        line_split = lines.strip.split
        action = line_split[1]
        expect(['accepts', 'recommends']).to include(action)
      end
    end

    it 'returns invalid if there is no required format for recommendation' do
      File.foreach(file.path).each do |lines|
        line_split = lines.strip.split
        action = line_split[1]
        case action
        when 'recommends'
          a = line_split.length
          expect(a).to eql(3)
        end
      end
    end

    it 'returns invalid if there is no required format for accepting' do
      File.foreach(file.path).each do |lines|
        line_split = lines.strip.split
        action = line_split[1]
        case action
        when 'accepts'
          a = line_split.length
          expect(a).to eql(2)
        end
      end
    end
  end
end