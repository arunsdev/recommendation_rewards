require 'faker'

FactoryGirl.define do
  factory :user do |f|
  	f.id { 1 }
    f.name { Faker::Name.name }
    f.points { 20 }
    f.is_accepted { true }
  end
end