require 'rails_helper'

describe User do
  it 'has a valid user' do
    user = FactoryGirl.create(:user)
    expect(user).to be_valid
  end
  it 'is invalid without a name' do
    user = FactoryGirl.build(:user, name: nil)
    expect(user).to_not be_valid
  end
  it 'is invalid without an id' do
    user = FactoryGirl.build(:user, id: nil)
    expect(user).to_not be_valid
  end
  it 'is invalid without points' do
    user = FactoryGirl.build(:user, points: nil)
    expect(user).to_not be_valid
  end
  it 'is invalid without acceptance' do
    user = FactoryGirl.build(:user, is_accepted: nil)
    expect(user).to_not be_valid
  end
end