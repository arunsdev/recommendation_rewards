require 'rails_helper'

describe 'recommendation/index.html.erb' do
  it "displays the form" do
    render :partial => "recommendation/form.html.erb"
  end

  it "renders the HTML template" do
    render

    expect(rendered).to match 'form'
  end
end