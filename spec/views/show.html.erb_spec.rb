require 'rails_helper'

describe 'recommendation/show.html.erb' do
  before do
    assign(:form, 'The points of the users are as follows...')
    render
  end

  it "displays the form" do
    render :partial => "recommendation/form.html.erb"
  end

  it "renders the HTML template" do
    render

    expect(rendered).to match 'form'
  end

  it 'displays the points of the users' do
  	expect(rendered).to include 'The points of the users are as follows...'
  end
end