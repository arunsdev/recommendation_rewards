class AddRecommendingUserToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :recommending_user, foreign_key: true
  end
end
