class IsAcceptedToUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :is_accepted, :boolean
  end
end
