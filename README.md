Clone the project in your local system.

`git clone git@gitlab.com:arunsdev/recommendation_rewards.git` 

Then do 
`bundle install` 
to install all gems.

**Dependencies**

Ruby version 2.3 or higher.

Rails version 5.0 or higher.

**Database creation & initialization**

Using default sqlite3 database for this project

*Steps to setup:*

`rails db:create` - To create the default and simple database.

`rails db:migrate` - To create the tables and associations.

`schema.rb` file will be created.

Now run the application `rails s` in the terminal.

Go to `http://localhost:3000`

**Test Suites**

Using RSpec as testing tool

*Steps to run:*

`rpsec spec/controllers/recommendation_controller_spec.rb` - To run the test for the controller.

`rpsec spec/models/user_spec.rb` - To run the test for the model.

Created sample text file in `rpsec spec/fixtures/test.txt`.

Used Factory girl to build the database for the user model `rpsec spec/factories/users.rb`

**Assumptions made while developing**

Inputs have been considered in chronological order top to bottom as old to latest respectively.

Input file will accepted with .txt format. Refer ('spec/fixtures/test.txt') for the format.

Users have been created through the inputs given and will be saved in database and will not be cleared if the new file input has been given.

